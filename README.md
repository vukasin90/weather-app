#custom message

Since I wasn't able to do all the things from the task, I added something extra, to compensate. I hope you'll like it. :)
I gotta say, I didn't expect an Angular project. Before this, I literally have just started to learn Angular, like few days before, so I had to learn quick and on the fly.
Therefore, I wasn't able to successfully finish everything. But, I'm happy with what I have done. Hope you'll be too :)

If something seems odd, or missing...please let me know. I never used GitLab (I have GitHub account), so I'm not sure if I've merged everything right.
It should be ok, but just in case to mention. 

# WeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
